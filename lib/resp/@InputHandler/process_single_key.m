function response = process_single_key(obj, answer)
switch obj.responder
    case 'simpleKeypressRobot'
        obj.inputemu('key_normal', answer);
end
obj = check_keys(obj);
response = KbName(obj.keys_pressed);
end

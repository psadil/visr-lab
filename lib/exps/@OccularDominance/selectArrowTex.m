function arrowTex = selectArrowTex(stims, correct_direction)
switch correct_direction
    case '\RIGHT'
        arrowTex = stims(1).tex;
    case '\LEFT'
        arrowTex = stims(2).tex;
end
end

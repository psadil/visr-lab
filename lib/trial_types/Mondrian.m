classdef Mondrian < BCFS
    
    properties
        prompter
        bcfs_keys
        paser
    end
    
    methods
        function obj = Mondrian(data, window, prompter, responder)
            obj@BCFS(data, window, responder);
            
            obj.bcfs_keys = obj.keys.bCFS;
            
            obj.prompter = prompter;
            
            obj.paser = PASHandler(responder);
            
        end
        
        function texture = make_texes(obj)
            
            object = obj.data.main.object(obj.trial);
            pair = obj.data.main.pair(obj.trial);
            
            % grab all images
            [im, ~, alpha] = imread(fullfile(pwd, 'stims', 'objects', 'png', ...
                sprintf('object-%03d_paired-%03d_ap-0.png',object, pair)));
            
            % setup with alpha layer
            image = cat(3,im,alpha);
            
            % make textures of images
            texture = Screen('MakeTexture', obj.window.pointer, image);
        end
        
        function draw_stim(obj, prompt, image_tex, flip)
            
            intensities = repmat(obj.truncnormrnd([1, obj.n_squares],.5, 10, 0, 1), [3, 1]);
            positions = obj.make_mondrian_positions();
            
            for eye = 1:2
                Screen('SelectStereoDrawBuffer', obj.window.pointer,eye-1);
                
                % draw same masking image to both eyes
                Screen('FillRect', obj.window.pointer, intensities, positions);
                
                if obj.data.main.eyes{obj.trial}(eye)
                    Screen('DrawTexture', obj.window.pointer, image_tex,[],...
                        obj.image_dst_rect, [], [], obj.data.flut.alpha(flip));
                end
                
                % always draw central fixation cross
                obj.drawFixation();
                
                % prompt participant to respond
                DrawFormattedText(obj.window.pointer, prompt, 'center', obj.text_note_placement);
            end
            Screen('DrawingFinished', obj.window.pointer);
        end
        
        
        function give_feedback(obj)
            
            switch obj.data.main.exit_flag{obj.trial}
                % CAUGHT occurs when participant responded very quickly and
                % yet an object was going to appear
                case 'CAUGHT'
                    if strcmp(obj.data.main.response{obj.trial}, 'j')
                        obj.elicitPAS;
                    else
                        obj.data.main.pas{obj.trial} = '0';
                    end
                    obj.prompter.show({'INCORRECT! An object was appearing.'});
                case 'OK'
                    switch obj.data.main.response{obj.trial}
                        case 'j'
                            obj.elicitPAS;
                            switch obj.data.main.condition{obj.trial}
                                case 'CFS'
                                    obj.prompter.show({'Correct! An object was appearing.'});
                                case 'Not Studied'
                                    obj.prompter.show({'INCORRECT! No object was going to appear.'});
                            end
                        case 'f'
                            obj.data.main.pas{obj.trial} = '0';
                            switch obj.data.main.condition{obj.trial}
                                case 'CFS'
                                    obj.prompter.show({'INCORRECT! An object was appearing.'});
                                case 'Not Studied'
                                    obj.prompter.show({'Correct! No object was going to appear.'});
                            end
                        case ''
                            switch obj.data.main.condition{obj.trial}
                                case 'CFS'
                                    obj.prompter.show({ 'There was an item! Please try to find it sooner.'});
                                case 'Not Studied'
                                    obj.prompter.show({'There was no item! Please stop searching sooner.'});
                            end
                    end
            end
            
        end
        
    end
    
    methods (Access = private)
        function elicitPAS(obj)
            vbl = GetSecs;
            
            for eye = 0:1
                Screen('SelectStereoDrawBuffer', obj.window.pointer, eye);
                
                % not that 'no image detected - 0\n',... implicit in
                % participants' response of 'f' during study phase
                DrawFormattedText(obj.window.pointer,['possibly saw, couldn''t name - 1\n',...
                    'definitely saw, but unsure what it was (could possibly guess) - 2\n',...
                    'definitely saw, could name - 3\n'],...
                    'center', 'center');
                
                DrawFormattedText(obj.window.pointer, '[Use the keypad to indicate your response]', ...
                    'center', obj.window.winRect(4)*.8);
            end
            Screen('Flip', obj.window.pointer, vbl + obj.window.ifi/2 );
            obj.data.main.pas{obj.trial} = obj.paser.waitFor(obj.keys.pas, '2');
            
        end
    end
    
end


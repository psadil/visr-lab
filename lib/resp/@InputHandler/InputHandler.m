classdef InputHandler < handle
    %InputHandler receive and parse inputs of various kinds from different
    %sources
    
    properties (Constant)
        device = [];
        keys = Keys;
    end
    
    properties (Abstract)
        end_trial_key
    end
    
    properties
        press_times = [];
        keys_pressed = [];
        rt = [];
        trial_ender = false;
        responder
    end
    
    properties (SetObservable)
        response = '';
    end
    
    methods
        function obj = InputHandler(responder)
            obj.responder = responder;            
        end
        
        response = process_single_key(obj, answer)
        obj = process_resp(obj, answer, trial_start)
        prepare(obj, codes)
        shutdown(obj)
        waitForSpace(obj)
        response = waitFor(obj, whichkeys, inputemucode)
        
    end
    
    methods (Access = protected)
        
        obj = check_keys(obj)
        response = clean_response(obj)
        rt = calc_rt(obj, trial_start)
        
    end
    
    methods (Static)
        inputemu(varargin)
    end
    
    
end


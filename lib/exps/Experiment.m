classdef Experiment < handle
    %Experiment Abstract collection of info about general experiment
    
    % - inhereting from handle means that there is only ever a single
    % 'experiment' in any given session.
    % - info is saved upon deletion of concrete subclass
    
    properties (Transient)
        flut_basename
        lib_dir
    end
    
    properties
        prompter;
        
        savepath
        subject
        seed
        subDir
        input
        exp_end
        window
        
        % values reset by debuglevel
        iti_sec = 0;
        max_cfs_dur_sec = 30;
        exp_start
        datetaken
        version
        
    end
    
    properties (Abstract)
        data
        exp
    end
    
    methods
        function obj = Experiment(input, askforsubnum)
            if nargin > 0
                rng('shuffle');
                scurr = rng; % set up and seed the randon number generator
                obj.seed = scurr.Seed;
                
                obj.exp_start = GetSecs; % record the time the experiment began
                obj.datetaken = date();
                obj.version = Experiment.get_version_string('version.txt');
                
                obj.input = input;
                
                obj.lib_dir = fullfile(obj.input.root, 'lib');
                
                
                % add libraries to path
                path(path, obj.input.root);
                path(path, genpath(obj.lib_dir));
                
                
                % Define the location of some directories we might want to use
                switch obj.input.responder
                    case 'user'
                        obj.savepath = fullfile(obj.input.root,'analyses','data');
                    otherwise
                        obj.savepath = fullfile(obj.input.root,'analyses','robo');
                end
                
                if askforsubnum
                    
                    % instantiate the subject number validator function
                    subjectvalidator = obj.makeSubjectDataChecker(obj.savepath);
                    
                    % -------- GUI input option ----------------------------------------------------
                    % call gui for input
                    guiInput = getSubjectInfo('subject', struct('title', 'Participant Number', 'type', 'textinput',...
                        'validationFcn', subjectvalidator));
                    if isempty(guiInput)
                        return
                    else
                        input = filterStructs(guiInput, input);
                    end
                    obj.subject = str2double(input.subject);
                    
                    
                    if strcmp(input.responder,'user') && input.debugLevel == 0
                        demographics(fullfile('analyses','data','demographics'));
                    end
                else
                    obj.subject = input.subject;
                end
                
                
                
                % now that there is a safe subject number, proceed to
                % store this directory for saving files
                obj.subDir = fullfile(obj.savepath, sprintf('sub-%03d', obj.subject), 'beh');
                if ~exist(obj.subDir, 'dir')
                    mkdir(obj.subDir);
                end
                
                % only open window after handling input diaglogue box
                obj.window = Window(obj.input);
                obj.prompter = Prompter(obj.window, obj.input.responder);
                
                
                % grab File Look Up Table (flut)
                obj.flut_basename = fullfile('stims', 'fluts',...
                    sprintf('sub-%03d_', obj.subject));
                
                switch input.debugLevel
                    case 1
                        obj.max_cfs_dur_sec = 30; % max duration until arrows are at full contrast
                end
            end
            
        end
        
        % NOTE: although super convenient to be able to auto delete this
        % object when it's done, making sure to save it and the data, this
        % could easily bite you! Be careful about when loading the data
        % back up because deleting again will try to save again!
        %
        % should probably implement some kind of loadobj saveobj methods
        function delete(obj)
            saveDir = fullfile(obj.subDir);
             if ~exist(saveDir, 'dir')
                mkdir(saveDir);
             end
                obj.exp_end = GetSecs;                
                fNamePrefix = fullfile(saveDir, ['sub-', num2str(obj.subject, '%03d'), '_task-', obj.exp]);                
             if ~exist([fNamePrefix,'_','exp','.mat'], 'file')                                
                writetable(obj.data.main, [fNamePrefix,'_data.csv']);
                writetable(obj.data.flut, [fNamePrefix,'_flut.csv']);
                
                save([fNamePrefix,'_','exp','.mat'], 'obj');                
            end
        end
        
        
    end
    
    
    methods (Abstract)
        obj = run(obj)
        give_instructions(obj)
    end
    
    methods (Access = protected, Static = true)
        function overwriteCheck = makeSubjectDataChecker(directory)
            % makeSubjectDataChecker function closer factory, used for the purpose
            % of enclosing the directory where data will be stored. This way, the
            % function handle it returns can be used as a validation function with getSubjectInfo to
            % prevent accidentally overwritting any data.
            function [valid, msg] = subjectDataChecker(subnum, ~)
                % the actual validation logic
                valid = false;
                
                % directories often reused, so search for run folder
                dirPathGlob = fullfile(directory, ['sub-', num2str(str2double(subnum), '%03d')]);
                if exist(dirPathGlob,'dir')
                    
                    msg = strjoin({'Data file for Subject', subnum, 'already exists!'}, ' ');
                    return
                else
                    valid = true;
                    msg = 'ok';
                end
            end
            
            overwriteCheck = @subjectDataChecker;
        end
        
    end
    
    methods (Access = private, Static = true)
        
        
        function version = get_version_string(fname, fallback)
            
            % The 'fname' argument should be a character array specifying a file name that can be
            % opened by fopen. The file should contain a single line specifying the
            % version string, any data beyond the first line is ignored.
            
            % If the file does not exist, cannot be opened in read mode, or is empty,
            % then the version string is set to the value specified in the optional 'fallback'
            % argument. The default value for the fallback version string is 'dev'.
            
            if nargin == 1
                fallback = 'dev';
            end
            
            not_found = false;
            unable_to_read = false;
            
            if exist(fname, 'file')
                try
                    fileID = fopen(fname,'r');
                    try
                        version = fgetl(fileID);
                        if version == -1
                            error('File %s is empty', fname);
                        else
                            next_line = fgetl(fileID);
                            if ischar(next_line)
                                warning('Ignoring all data in file %s past first line', fname);
                            end
                        end
                        fclose(fileID);
                    catch
                        unable_to_read = true;
                        fclose(fileID);
                    end
                catch
                    not_found = true;
                end
            else
                not_found = true;
            end
            
            if not_found || unable_to_read
                version =  fallback;
            end
            
            if not_found
                warning('File %s not found, setting version string to ''%s''', fname, version);
            end
            
            if unable_to_read
                warning('Unable to read version string from file, setting version string to %s', fname, version);
            end
        end
        
    end
    
end


classdef PASHandler < InputHandler
    %PASHandler receive and parse PAS responses
    
    properties
        end_trial_key
    end
    
    methods
        function obj = PASHandler(responder)
            obj@InputHandler(responder);
            obj.end_trial_key = {KbName(obj.keys.pas)};
        end
                        
    end
    
    
end


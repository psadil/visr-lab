classdef Keys < handle
    %Keys Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        escape
        bCFS
        space
        arrows
        enter
        pas
        name
        bkspace
    end
    
    methods
        function obj = Keys()
            codes = zeros(1,256);
            obj.escape = codes;
            obj.escape(KbName('ESCAPE')) = 1;
            obj.bCFS = codes;
            obj.bCFS(KbName({'f','j'})) = 1;
            obj.space = codes;
            obj.space(KbName({'space'})) = 1;
            
            obj.arrows = codes;
            obj.arrows(KbName({'4','6','RightArrow','LeftArrow'})) = 1;
            
            obj.enter = codes;
            obj.enter(KbName({'return'})) = 1;
            obj.pas = codes;
            obj.pas(KbName({'1','2','3','1!','2@','3#'})) = 1;
            obj.name = codes;
            obj.name(KbName({'a','b','c','d','e','f','g','h','i','j','k','l','m', ...
                'n','o','p','q','r','s','t','u','v','w','x','y','z'})) = 1;
            obj.bkspace = codes;
            obj.bkspace(KbName({'BackSpace'})) = 1;
        end
        
    end
    
end


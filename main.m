function main(varargin)

warning('off','MATLAB:nargchk:deprecated');
%% collect input
% use the inputParser class to deal with arguments
ip = inputParser;
addParameter(ip, 'subject', 1, @isnumeric);
addParameter(ip, 'dominanteye', 'right', @(x) sum(strcmp(x, {'left','right'}))==1);
addParameter(ip, 'debugLevel', 0, @(x) any(x==[0, 1, 10]));
addParameter(ip, 'responder', 'user', @(x) sum(strcmp(x, {'user', 'simpleKeypressRobot'}))==1);
addParameter(ip, 'experiments', [1, 1], @(x) length(x)==2);
addParameter(ip, 'study', true, @(x) islogical(x));
addParameter(ip, 'naming', true, @(x) islogical(x));
addParameter(ip, 'refreshRate', 100, @(x) any(x==[100, 60]));
addParameter(ip, 'stereomode', 1, @(x) any(x==[0, 1]));
addParameter(ip, 'SkipSyncTests', 0, @(x) any(x==[0, 1, 2]) );
addParameter(ip, 'root', pwd, @ischar );
parse(ip,varargin{:});
input = ip.Results;

addpath(genpath('lib'))

%% setup


PsychDefaultSetup(2);

if input.experiments(1)
    %% assess occular dominance
    occulardominance = OccularDominance(input, true);
    occulardominance = occulardominance.run();
    
    % when occulardominance check was run, override whatever value was
    % input.
    input.dominanteye = occulardominance.DominantEye();
    input.subject = occulardominance.subject;
    
    % deletes to force save + clear data and flut properties
    % note that I wouldn't need to do this if the data were actually
    % just a connection
    delete(occulardominance);
end

%% run main experiment
if input.experiments(2)
    if input.experiments(1)==0
        askforsubnum = true;
    else
        askforsubnum = false;
    end
    visualrecollection = VisualRecollection(input, askforsubnum );
    visualrecollection.run();
    
    prop_names_correct = visualrecollection.prop_names_attempted();
    
    visualrecollection.prompter.show({sprintf(['Thanks for participating! \n',...
        'You attempted to name %2g%% of the objects. You''ve earned an extra $%g. \n',...
        'Please leave this screen open and go find the researcher.'], prop_names_correct*100, ceil(prop_names_correct*5))});
    delete(visualrecollection);
end


end
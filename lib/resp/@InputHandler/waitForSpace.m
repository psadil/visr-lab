function waitForSpace(obj)
obj.prepare(obj.keys.space)
while 1
    pressed = obj.process_single_key('\SPACE');
    if ~isempty(pressed)
        obj.keys_pressed = [];
        obj.press_times = [];
        break;
    end
end
obj.shutdown();
end

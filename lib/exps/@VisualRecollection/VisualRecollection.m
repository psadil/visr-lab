classdef VisualRecollection < Experiment
    %VisualRecollection Experimental logic for visr
    
    
    properties
        exp = 'visr';
        data
        
        % different kinds of tasks that participants will encounter in this
        % experiment
        mondrian
        binocular
        namer
        cuer
        noise
        
        cue_dur_sec = .1
        name_dur_sec = 60
        
        n_list
        n_study_rep
        n_trial
        
    end
    
    properties (Dependent)
        prop_names_attempted
        prop_names_correct
    end
    
    methods
        function obj = VisualRecollection(input, askforsubnum)
            obj@Experiment(input, askforsubnum);
            input.subject = obj.subject;
            
            obj.data = Data(input, obj.flut_basename, obj.exp);
            
            obj.n_trial = max(obj.data.main.trial);
            obj.n_list = max(obj.data.main.list);
            obj.n_study_rep = max(obj.data.main.rep);
            
            obj.mondrian = Mondrian(obj.data, obj.window, obj.prompter, obj.input.responder);
            obj.binocular = Binocular(obj.data, obj.window, obj.prompter, obj.input.responder);
            obj.namer = StaticImage(obj.data, obj.window, obj.prompter, 'name', obj.input.responder);
            obj.cuer = StaticImage(obj.data, obj.window, obj.prompter, 'cue', obj.input.responder);
            
            obj.cuer.duration_sec = obj.cue_dur_sec;
            obj.namer.duration_sec = obj.name_dur_sec;
            
            obj.noise = Noise(obj.data, obj.window, obj.prompter, obj.input.responder);
            
        end
        
        function prop_names_attempted = get.prop_names_attempted(obj)
            %prop_names_attempted: what proportion of naming trials did the
            %participant respond to?
            
            data_naming = obj.data.main.response(strcmpi(obj.data.main.phase, 'name'),:);
            prop_names_attempted = ceil(mean(~cellfun(@isempty, data_naming))*10)/10;
        end


        function prop_names_correct = get.prop_names_correct(obj)
            %prop_names_correct: what proportion of naming trials did the
            %participant get correct?
            % value returned as percentage, rounded up to nearest 10
                        
            targets = readtable(fullfile(obj.input.root, 'stims', 'objects', 'names.csv'));
            
            name_trials = strcmpi(obj.data.main.phase, 'name');
            responses = [obj.data.main(name_trials,'object'), obj.data.main(name_trials,'response')];
            responses.correct = NaN(size(responses,1), 1);
            for trial = 1:size(responses,1)
                responses.correct(trial) = any(strcmpi(responses.response{trial}, targets{responses.object(trial),:}));
            end
            
            prop_names_correct = ceil(nanmean( responses.correct)*10)*10;
        end

        
        %% main experimental loop
        obj = run(obj);
    end
    
    methods (Access = private)
        % these functions could be a get method, but that would require
        % setting up the object such that trial, rep, phase, and list are
        % all properties.
        function index_data = calc_index_data(obj, trial, rep, phase, list)
            
            index_data = obj.data.main.trial==trial & ...
                obj.data.main.rep == rep & ...
                strcmpi(obj.data.main.phase, phase) & ...
                obj.data.main.list == list;
        end
        
        function index_flut = calc_index_flut(obj, trial, rep, phase, list)
            
            % need to use find rather than logical indexing
            % due to indexing of flut within run
            index_flut = find(obj.data.flut.trial == trial & ...
                obj.data.flut.rep == rep & ...
                strcmpi(obj.data.flut.phase, phase) & ...
                obj.data.flut.list == list);
        end
                
    end
    
    
end


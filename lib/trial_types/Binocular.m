classdef Binocular < BCFS
    %Binocular BCFS trial that only listens for 'escape' key
    
    properties
        dst_rect
        prompter
        bcfs_keys
    end
        
    methods
        function obj = Binocular(data, window, prompter, responder)
            obj@BCFS(data, window, responder);
            
            obj.bcfs_keys = obj.keys.escape;
            
            obj.prompter = prompter;
            
        end
                
        
        function texture = make_texes(obj)
            
            object = obj.data.main.object(obj.trial);
            pair = obj.data.main.pair(obj.trial);
            
            % grab all images
            [im, ~, alpha] = imread(fullfile(pwd, 'stims', 'objects', 'png', ...
                sprintf('object-%03d_paired-%03d_ap-0.png',object, pair)));
            
            % setup with alpha layer
            image = cat(3,im,alpha);
            
            % make textures of images
            texture = Screen('MakeTexture', obj.window.pointer, image);
        end
        
        function draw_stim(obj, prompt, image_tex, flip)
            
            intensities = repmat(obj.truncnormrnd([1, obj.n_squares],.5, 10, 0, 1), [3, 1]);
            positions = obj.make_mondrian_positions();
                        
            for eye = 1:2
                Screen('SelectStereoDrawBuffer', obj.window.pointer,eye-1);
                
                % draw same masking image to both eyes
                Screen('FillRect', obj.window.pointer, intensities, positions);
                
                Screen('DrawTexture', obj.window.pointer, image_tex,[], obj.noise_dst_rect, [], [], obj.data.flut.alpha(flip));
                
                % always draw central fixation cross
                obj.drawFixation();
                
                % prompt participant to respond
                DrawFormattedText(obj.window.pointer, prompt, 'center', obj.text_note_placement);
            end
            Screen('DrawingFinished', obj.window.pointer);            
        end
        
        function give_feedback(~)
            % do nothing
        end
        
        
    end
    
end


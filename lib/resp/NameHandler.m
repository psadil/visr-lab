classdef NameHandler < InputHandler
    %InputHandler receive and parse inputs of various kinds from different
    %sources
    
    
    properties
        end_trial_key = {'Return'};
    end
        
    methods
        function obj = NameHandler(responder)
            obj@InputHandler(responder);
        end
                        
    end
        
    
end


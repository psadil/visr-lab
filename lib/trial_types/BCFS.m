classdef BCFS < BaseStimulus
    %BCFS breaking continuous flash suppression trial
    %   Detailed explanation goes here
    
    properties
        data
        
        %% Mondrians
        
        % Maximum and minimum length of suppressor rects
        sqr_max_side_deg = 3;
        sqr_min_side_deg = 1;
        
        
        % number of squares to draw
        n_squares = 1000;
        
        sqr_max_side_pix
        sqr_min_side_pix
        
        handler_listener
        trial_listener
    end
    
    properties (Constant)
        keys = Keys
    end
    
    properties (SetObservable, AbortSet)
        bcfshandler
        trial = 1;
    end
    
    properties (Abstract)
        bcfs_keys
    end
    
    
    methods
        function obj = BCFS(data, window, responder)
            obj@BaseStimulus(window);
            
            obj.bcfshandler = BCFSHandler(responder);
            
            obj.handler_listener = addlistener(obj.bcfshandler, 'response',...
                'PostSet', @(src,evnt)BCFS.assign_resp(obj,src,evnt) );
            obj.trial_listener = addlistener(obj, 'trial',...
                'PostSet', @(src,evnt)BCFS.setup_new_trial(obj,src,evnt) );
            
            obj.data = data;
            
            
            % expecting max and min of 40 and 15
            obj.sqr_max_side_pix = floor(obj.deg2pix(obj.sqr_max_side_deg));
            obj.sqr_min_side_pix = floor(obj.deg2pix(obj.sqr_min_side_deg));
            
            
        end
        
        function obj = run(obj, index_flut, prompt)
            
            nflips = numel(index_flut);
            
            texture = make_texes(obj);
            
            obj.bcfshandler.prepare(obj.bcfs_keys);
            
            vbl = GetSecs;
            
            trial_start_set = false;
            for flip = 1:(nflips - 1)
                
                % for each flip, pick out one of the mondrians to draw
                draw_stim(obj, prompt, texture, index_flut(flip));
                
                % flip only in sync with mondrian presentation rate
                if flip == 1
                    [obj.data.flut.vbl_realized(index_flut(flip)), ~, ~, obj.data.flut.missed(index_flut(flip)) ] = ...
                        Screen('Flip', obj.window.pointer, ...
                        vbl + (obj.cycles_per_flip - obj.window.slack) * obj.window.ifi);
                else
                    [obj.data.flut.vbl_realized(index_flut(flip)), ~, ~, obj.data.flut.missed(index_flut(flip)) ] = ...
                        Screen('Flip', obj.window.pointer, ...
                        obj.data.flut.vbl_realized(index_flut(flip-1)) + ...
                        (obj.cycles_per_flip - obj.window.slack) * obj.window.ifi );
                end
                                
                if ~trial_start_set                    
                    % if the max alpha is low, set right away
                    if max(obj.data.flut.alpha(index_flut)) < 0.1
                        obj.data.main.trial_start(obj.trial) = obj.data.flut.vbl_realized(index_flut(flip));
                        trial_start_set = true;
                        % otherwise, only say trial has started after jitter
                        % has ended
                    elseif obj.data.flut.alpha(index_flut(flip)) > 0
                        obj.data.main.trial_start(obj.trial) = obj.data.flut.vbl_realized(index_flut(flip));
                        trial_start_set = true;
                    end
                end
                
                % note, RT will be nan if participant responds before trial
                % has officially started (there is a stimulus on screen or
                % the first flip has occurred and no stim will be shown on
                % this trial)
                obj.bcfshandler.process_resp(obj.data.main.answer{obj.trial}, obj.data.main.trial_start(obj.trial));
                
                if ~isempty(obj.data.main.response{obj.trial})
                    break;
                end
                
            end
            % flip once more to end trial. Next flip might occur after some
            % loading, so can't rely on that for clearing the display
            [obj.data.flut.vbl_realized(index_flut(flip+1)), ~, ~, obj.data.flut.missed(index_flut(flip+1)) ] = ...
                Screen('Flip', obj.window.pointer, ...
                obj.data.flut.vbl_realized(index_flut(flip)) + ...
                (obj.cycles_per_flip - obj.window.slack) * obj.window.ifi );
            
            obj.bcfshandler.shutdown();
            
            obj.data.main.trial_end(obj.trial) = obj.data.flut.vbl_realized(index_flut(flip+1));
            
            % finished displaying this image, so close it up
            Screen('Close', texture);
            
            if strcmpi(obj.data.main.response(obj.trial),'ESCAPE')
                obj.data.main.exit_flag(obj.trial) = {'ESCAPE'};
            elseif (obj.data.flut.alpha(index_flut(flip)) < 0.01) && (max(obj.data.flut.alpha(index_flut)) > 0.01)
                obj.data.main.exit_flag(obj.trial) = {'CAUGHT'};
            else
                obj.data.main.exit_flag(obj.trial) = {'OK'};
            end
            
            obj.assign_null_resp();
            
            give_feedback(obj);
            
            % final flip so that screen appears functional after
            % participant responds to feedback
            Screen('Flip', obj.window.pointer, ...
                GetSecs + (5 - obj.window.slack) * obj.window.ifi );
            
        end
        
        function assign_null_resp(obj)
            
            if isempty(obj.data.main.response{obj.trial})
                obj.handler_listener.Enabled = false;
                obj.data.main.response{obj.trial} = 'NULL';
                obj.handler_listener.Enabled = true;
            end
        end
        
    end
    
    methods (Abstract = true)
        texture = make_texes(obj);
        draw_stim(obj, prompt, texture, flip);
        give_feedback(obj);
    end
    
    methods (Static)
        function assign_resp(obj, ~, ~)
            % set based on keys_pressed rather than response, given that
            % response should be empty (final key press is always the
            % trial_end_key)
            obj.data.main.response(obj.trial) = {KbName(obj.bcfshandler.keys_pressed)};
            
            if obj.bcfshandler.trial_ender
                obj.data.main.rt(obj.trial) = obj.bcfshandler.rt;
                obj.data.main.exit_flag(obj.trial) = {KbName(obj.bcfshandler.keys_pressed)};
                
                obj.bcfshandler.trial_ender = false;
            end
            obj.bcfshandler.keys_pressed = zeros(0);
        end
        
        function setup_new_trial(obj, ~, ~)
            
            obj.handler_listener.Enabled = false;
            obj.bcfshandler.response = '';
            obj.handler_listener.Enabled = true;
        end
        
    end
    
    
    methods (Access = protected)
        
        function positions = make_mondrian_positions(obj)
            
            % positions of squares in mondrian
            
            positions(1,:) = randi([-obj.sqr_max_side_pix, obj.mondrian_size_pix],[1, obj.n_squares]);
            positions(2,:) = randi([-obj.sqr_max_side_pix, obj.mondrian_size_pix],[1, obj.n_squares]);
            positions(3,:) = min(positions(1,:,:) + repmat(obj.sqr_min_side_pix,[1, obj.n_squares]) +...
                randi(obj.sqr_max_side_pix - obj.sqr_min_side_pix, [1, obj.n_squares]), obj.mondrian_size_pix);
            positions(4,:) = min(positions(2,:,:) + repmat(obj.sqr_min_side_pix,[1, obj.n_squares]) +...
                randi(obj.sqr_max_side_pix - obj.sqr_min_side_pix, [1, obj.n_squares]), obj.mondrian_size_pix);
            
            % shift mondrians to center of screen
            shifts = CenterRect([0 0 obj.mondrian_size_pix - obj.sqr_max_side_pix obj.mondrian_size_pix - obj.sqr_max_side_pix], obj.window.winRect);
            shifts2 = repmat([shifts(1:2)'; shifts(1:2)'], [1, obj.n_squares]);
            
            positions = positions + shifts2;
            
        end
    end
    
    methods (Access = protected, Static=true)
        
        function z=truncnormrnd(N,mu,sig,xlo,xhi)
            % truncnormrnd: truncated normal deviate generator
            % usage:z=truncnormrnd(N,mu,sig,xlo,xhi)
            %
            % (assumes the statistics toolbox, its easy
            % to do without that toolbox though)
            %
            % arguments: (input)
            % N - size of the resulting array of deviates
            % (note, if N is a scalar, then the result will be NxN.)
            % mu - scalar - Mean of underlying normal distribution
            % sig - scalar - Standard deviation of underlying normal distribution
            % xlo - scalar - Low truncation point, if any
            % xhi - scalar - High truncation point, if any
            %
            % arguments: (output)
            % z - array of truncated normal deviates, size(z)==N
            
            % defaults
            if (nargin<2)||isempty(mu)
                mu=0;
            end
            
            if (nargin<3)||isempty(sig)
                sig=0;
            end
            
            if (nargin<4)||isempty(xlo)
                xlo=-inf;
                plo=0;
            else
                plo=normcdf((xlo-mu)/sig);
            end
            
            if (nargin<5)||isempty(xhi)
                xhi=inf;
                phi=1;
            else
                phi=normcdf((xhi-mu)/sig);
            end
            
            % test if trunation points are reversed
            if xlo>xhi
                error 'Must have xlo <= xhi if both provided'
            end
            
            % generate uniform [0,1] random deviates
            r=rand(N);
            
            % scale to [plo,phi]
            r=plo+(phi-plo)*r;
            
            % Invert through standard normal
            z=norminv(r);
            
            % apply shift and scale
            z=mu+z*sig;
            
        end
        
        
    end
    
    
end


function rt = calc_rt(obj, trial_start)

% RT is always based on most recent key pressed
rt = obj.press_times(obj.keys_pressed(end)) - trial_start;

end

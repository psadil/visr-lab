function [] = waitFor(forwhat,device,responder)

keycodes = zeros(1,256);
switch forwhat
    case 'space'
        keycodes(KbName({'space'})) = 1;
        inputcode = '\SPACE';

    case {'enter','return'}
        keycodes(KbName({'return'})) = 1;
        inputcode = '\ENTER';
end

KbQueueCreate(device, keycodes);
KbQueueStart(device);

while 1
    
    [keys_pressed, ~] = responder(device, inputcode);
    
    if ~isempty(keys_pressed)
        break;
    end
end

KbQueueStop(device);
KbQueueFlush(device);
KbQueueRelease(device);
end

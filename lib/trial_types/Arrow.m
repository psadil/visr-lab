classdef Arrow < BCFS
    %Arrow Defines arrow facing one direction presented to one eye
    %   Detailed explanation goes here
    
    properties
        offset_tip_pix
        offset_inner_pix
        dst_rect
        fromY
        toY
        prompter
        bcfs_keys
    end
    
    properties (Constant)
        penWidth = 10;
        offset_tip_deg = .4;
        offset_inner_deg = 4;
    end
    
    methods
        function obj = Arrow(data, window, prompter, responder)
            obj@BCFS(data, window, responder);
            
            obj.bcfs_keys = obj.keys.arrows;
            
            obj.prompter = prompter;
            
            obj.offset_tip_pix = obj.deg2pix(obj.offset_tip_deg);
            
            obj.offset_inner_pix = obj.deg2pix(obj.offset_inner_deg);
            obj.dst_rect([1,3]) = obj.noise_dst_rect([1,3]) + [obj.offset_inner_pix, - obj.offset_inner_pix];
            
            obj.fromY = obj.window.yCenter;
            obj.toY = obj.window.yCenter;
            
        end
        
        function texture = make_texes(obj)
            
            switch obj.data.main.answer{obj.trial}
                case '\LEFT'
                    fromX = obj.dst_rect(3);
                    toX = obj.dst_rect(1);
                    tipTopX = toX + obj.offset_tip_pix;
                case '\RIGHT'
                    fromX = obj.dst_rect(1);
                    toX = obj.dst_rect(3);
                    tipTopX = toX - obj.offset_tip_pix;
            end
            
            texture = Screen('MakeTexture', obj.window.pointer, ...
                ones(obj.window.winRect(4), obj.window.winRect(3)) .* obj.window.gray);
            
            % horizontal part
            Screen('DrawLine', texture, obj.window.black,...
                fromX, obj.fromY, ...
                toX, obj.toY, obj.penWidth);
            
            % upper head
            Screen('DrawLine', texture, obj.window.black, ...
                tipTopX, obj.toY - obj.offset_tip_pix, ...
                toX, obj.toY, obj.penWidth);
            
            % lower head
            Screen('DrawLine', texture, obj.window.black, ...
                tipTopX, obj.toY + obj.offset_tip_pix, ...
                toX, obj.toY, obj.penWidth);
        end
        
        function draw_stim(obj, prompt, image_tex, flip)
            
            intensities = repmat(obj.truncnormrnd([1, obj.n_squares],.5, 10, 0, 1), [3, 1]);
            positions = obj.make_mondrian_positions();
            
            for eye = 1:2
                Screen('SelectStereoDrawBuffer', obj.window.pointer,eye-1);
                
                % draw same masking image to both eyes
                Screen('FillRect', obj.window.pointer, intensities, positions);
                
                if obj.data.main.eyes{obj.trial}(eye)
                    Screen('DrawTexture', obj.window.pointer, image_tex, [],...
                        [], [], [], obj.data.flut.alpha(flip));
                end
                
                % always draw central fixation cross
                obj.drawFixation();
                
                % prompt participant to respond
                DrawFormattedText(obj.window.pointer, prompt, 'center', obj.text_note_placement);
            end
            Screen('DrawingFinished', obj.window.pointer);            
        end
        
        
        function give_feedback(obj)
            switch obj.data.main.exit_flag{obj.trial}
                case 'CAUGHT'
                    obj.prompter.show({'INCORRECT! Please wait until you are certain!'});
                otherwise
                    switch obj.data.main.response{obj.trial}
                        case 'RightArrow'
                            if strcmp('\RIGHT', obj.data.main.answer{obj.trial})
                                obj.prompter.show({'Correct!'});
                            else
                                obj.prompter.show({'INCORRECT! Please wait until you are certain!'});
                            end
                        case 'LeftArrow'
                            if strcmp('\LEFT', obj.data.main.answer{obj.trial})
                                obj.prompter.show({'Correct!'});
                            else
                                obj.prompter.show({'INCORRECT! Please wait until you are certain!'});
                            end
                    end
            end
            
        end
    end
    
    
end


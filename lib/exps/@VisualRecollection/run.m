function obj = run(obj)

for list = 1:obj.n_list
    
    if obj.input.study
        if list == 1
            give_instructions(obj);
        else
            obj.prompter.show([{['You are beginning list ', ...
                num2str(list-1), ' out of ', num2str(obj.n_list-1), ' lists in the main experiment']},...
                { 'Remember to keep your eyes focused on the cross in the center'}
                ]);
        end
        
        %% go through study phase of this list
        for rep = 1:obj.n_study_rep
            for trial = 1:obj.n_trial
                
                index_data = obj.calc_index_data(trial, rep, 'study', list);
                index_flut = obj.calc_index_flut(trial, rep, 'study', list);
                
                switch obj.data.main.condition{index_data}
                    case {'CFS', 'Not Studied'}
                        obj.mondrian.trial = index_data;
                        obj.mondrian.run(index_flut, ...
                            ['Press ''j'' if you see an object.\n',...
                            'If you haven''t seen an object and think that one won''t appear, press ''f''']);
                    case 'Binocular'
                        obj.binocular.trial = index_data;
                        obj.binocular.run(index_flut, ...
                            'Study the details of this object.');
                end
                
                % inter-trial-interval
                WaitSecs(obj.iti_sec);
            end
        end
    end
    
    %% Instruction for test
    if list == 1
        give_instructions(obj.namer);
    else
        obj.prompter.show({['This is the first test phase for list ', ...
            num2str(list), ' out of ', num2str(obj.n_list), ' lists\n',...
            'Please try to name the following objects.']});
    end
    
    %% go through first naming phase of list
    if obj.input.naming
        for trial = 1:obj.n_trial
            
            obj.namer.trial = obj.calc_index_data(trial, 1, 'name', list);
            obj.namer.run('This is a(n): ');
        end
    end
    
    
    %% Instruction for (cue + go/nogo)
    if list == 1
        give_instructions(obj.noise);
    else
        obj.prompter.show([{['This is the second test phase for list ', num2str(list),...
            ' out of ', num2str(obj.n_list), ' lists']},...
            {'Remember to keep your eyes focused on the center cross'}
            ]);
    end
    
    %% full test (cue + go/nogo)
    for trial = 1:obj.n_trial
        
        index_data = obj.calc_index_data(trial, 1, 'noise', list);
        
        if strcmpi(obj.data.main.cue{index_data}, 'present')
            obj.cuer.trial = index_data;
            obj.cuer.run('Think about which object is hidden.');
        end
        
        obj.prompter.show({'Get ready to respond...'});
        switch obj.data.main.gonogo{index_data}
            case {'go', 'nogo'}
                obj.noise.trial = index_data;
                index_flut = obj.calc_index_flut(trial, 1, 'noise', list);
                obj.noise.run(index_flut, 'Press Enter if an object is present.');
            case 'name'
                obj.namer.trial = obj.calc_index_data(trial, 2, 'name', list);
                obj.namer.run('This is a(n): ');
        end
        
        % inter-trial-interval
        WaitSecs(obj.iti_sec);
    end
end


%% close up
obj.prompter.show({['That is the end of the experiment.\n',...
    'Thanks for participating! Please find the experimenter.']});

end

function response = clean_response(obj)

% arrow keys are weird
if any(obj.keys_pressed == 102)
    obj.keys_pressed = 39; %RightArrow
elseif any(obj.keys_pressed == 100)
    obj.keys_pressed = 37; %LeftArrow
else
    obj.keys_pressed = obj.keys_pressed(end);
end

switch obj.keys_pressed
    case num2cell(KbName(obj.end_trial_key))
        response = obj.response;
        obj.trial_ender = true;
    case KbName('BackSpace')
        if ~isempty(obj.response)
            response = obj.response(1:end-1);
        else
            response = obj.response;
        end
    case KbName('Space')
        response = [obj.response, ' '];
    otherwise
        response = [obj.response, KbName(obj.keys_pressed)];
end
end
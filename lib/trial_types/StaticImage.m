classdef StaticImage < BaseStimulus
    
    properties
        duration_sec = 120;
        data
        prompter
        
        handler_listener
        trial_listener
        responsekeys
        name_ender
    end
    
    properties (Constant)
        keys = Keys
    end
    
    properties (SetObservable, AbortSet)
        namehandler
        trial = 1;
    end
    
    methods
        function obj = StaticImage(data, window, prompter, keytype, responder)
            obj@BaseStimulus(window);
            
            obj.namehandler = NameHandler(responder);
            obj.prompter = prompter;
            
            obj.handler_listener = addlistener(obj.namehandler, 'response',...
                'PostSet', @(src,evnt)StaticImage.assign_resp(obj,src,evnt) );
            obj.trial_listener = addlistener(obj, 'trial',...
                'PostSet', @(src,evnt)StaticImage.setup_new_trial(obj,src,evnt) );
            
            obj.data = data;
            switch keytype
                case 'name'
                    obj.responsekeys = obj.keys.name + ...
                        obj.keys.escape + obj.keys.bkspace + ...
                        obj.keys.space + obj.keys.enter;
                case 'cue'
                    obj.responsekeys = obj.keys.escape;
            end
        end
        
        function obj = run(obj, prompt)
            
            texture = make_texes(obj);
            
            obj.namehandler.prepare(obj.responsekeys);
            
            % Initial timestamp on which to base subsequent flips
            vbl = GetSecs;
            obj.data.main.trial_start(obj.trial) = vbl;
            
            while vbl - obj.data.main.trial_start(obj.trial) < obj.duration_sec
                
                % only thing changing is the current value of response
                draw_stim(obj, prompt, obj.data.main.response{obj.trial}, texture);
                
                % try to be precise about flips to avoid missed flip
                % warnings, but only flip every so often to allow responses
                % to get processed
                vbl = Screen('Flip', obj.window.pointer, ...
                    vbl + (5 - obj.window.slack) * obj.window.ifi );
                
                obj.namehandler.process_resp('\ENTER', obj.data.main.trial_start(obj.trial));
                
                if any(strcmpi(obj.data.main.exit_flag(obj.trial), obj.namehandler.end_trial_key ))
                    break;
                end
                
            end
            % one final flip to mark end of trial
            obj.data.main.trial_end(obj.trial) = Screen('Flip', obj.window.pointer, ...
                    vbl + (5 - obj.window.slack) * obj.window.ifi );
                
            obj.namehandler.shutdown();
            
            % finished displaying this image, so close it up
            Screen('Close', texture);
            
        end
        
        
        function texture = make_texes(obj)
            
            object = obj.data.main.object(obj.trial);
            pair = obj.data.main.pair(obj.trial);
            
            % grab all images
            [im, ~, alpha] = imread(fullfile(pwd, 'stims', 'objects', 'png', ...
                sprintf('object-%03d_paired-%03d_ap-1.png',object, pair)));
            
            % setup with alpha layer
            image = cat(3,im,alpha);
            
            % make textures of images
            texture = Screen('MakeTexture', obj.window.pointer, image);
        end
        
        function draw_stim(obj, prompt, response, image_tex)
            
            for eye = 1:2
                Screen('SelectStereoDrawBuffer', obj.window.pointer,eye-1);
                
                % draw 'paper' which will appear to occlude whole object
                Screen('FillRect', obj.window.pointer,  obj.window.gray*.5, obj.noise_dst_rect);

                Screen('DrawTexture', obj.window.pointer, image_tex, [], obj.image_dst_rect);
                
                % prompt participant to respond
                [nx, ny] = DrawFormattedText(obj.window.pointer, prompt, 'center', obj.text_note_placement);
                
                % show the response that they've given
                DrawFormattedText(obj.window.pointer, response, nx, ny);
            end
            Screen('DrawingFinished', obj.window.pointer);
        end
        
        function give_instructions(obj)
            obj.prompter.show([{'That was all of the objects. You have reached the memory test!'} ,...
                {['You will now see images of objects.\n',...
                'However, only a small part of those objects will be visible.\n',...
                'Some of these parts will come from objects that you studied, but others will be new.\n',...
                'Your task will be to identify the partially hidden objects.\n',...
                'Feel free to use your memory of the objects you studied if you think it will help.\n',...
                '\nAn example will be shown on the next slide.']},...
                {'Type your responses with the keyboard, and press Enter when you have finished typing.'}
                ]);
            
            obj.trial = strcmpi(obj.data.main.expt, 'instructions') & ...
                strcmpi(obj.data.main.phase, 'name');
            obj.run('This is a(n): ');
            
            obj.prompter.show([{'In that case, the correct answer would have been ''lightswitch'''},...
                {'Please name the following objects'}]);
            
        end
        
    end
    
    methods (Static)
        function assign_resp(obj, ~, ~)
            obj.data.main.response(obj.trial) = {obj.namehandler.response};
            
            if obj.namehandler.trial_ender
                obj.data.main.rt(obj.trial) = obj.namehandler.rt;
                obj.data.main.exit_flag(obj.trial) = {KbName(obj.namehandler.keys_pressed)};
                                
                obj.namehandler.trial_ender = false;
            end
            obj.namehandler.keys_pressed = zeros(0);
            
        end
        
        function setup_new_trial(obj, ~, ~)
            
            obj.handler_listener.Enabled = false;
            obj.namehandler.response = '';
            
            obj.handler_listener.Enabled = true;
        end
    end
    
    
end


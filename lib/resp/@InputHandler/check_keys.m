function obj = check_keys(obj)
% The response string and RT vector are returned (updated with any input
% given by the participant) as well as the advance and redraw flags.

% Check the KbQueue for presses
[ pressed, timespressed] = KbQueueCheck(obj.device);
if pressed
    % find the keycode for the keys pressed since last check
    whichpressed = find(timespressed);
    % sort the recorded press time to find their linear position
    [~, ind] = sort(timespressed(timespressed~=0));
    % Arrange the keycodes according to the order they were pressed
    obj.keys_pressed = whichpressed(ind);
    obj.press_times = timespressed;
end
end

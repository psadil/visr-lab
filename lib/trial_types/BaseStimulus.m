classdef BaseStimulus < handle
    %BaseStimulus SuperClass, not typically instantiated
    %   Detailed explanation goes here
    
    % transient because don't need to save in this structure
    properties (Transient = true)
        window
    end
    
    properties
        
        %% Fixations
        fixSize_deg = 1;
        fixSize_pix
        
        fixLineSize = 2;
        fix_color
        
        % cue point coordinates
        fix_rect
        
        %% Stimulus placement
        
        image_size_deg = 13;
        
        % expecting 300 x 300
        image_size_pix
        
        image_dst_rect
        
        noise_size_deg
        noise_size_pix
        noise_dst_rect
        
        % size of bounding square around mondrian mask
        % needs to be larger than the stimulus size
        mondrian_size_deg = 20;
        mondrian_size_pix
        
        
        %% Text Placement
        text_note_placement
        
        flip_hz = 10
        cycles_per_flip
        
    end
    
    methods
        function obj = BaseStimulus(window)
            obj.window = window;
            
            obj.fixSize_pix = obj.deg2pix(obj.fixSize_deg);
            
            obj.fix_color = obj.window.white;
            
            % cue point coordinates
            obj.fix_rect = ...
                [[[-obj.fixSize_pix/2, obj.fixSize_pix/2];[0,0]],...
                [[0,0];[-obj.fixSize_pix/2, obj.fixSize_pix/2]]];
            
            obj.image_size_pix = obj.deg2pix(obj.image_size_deg);
            
            obj.image_dst_rect = CenterRect([0 0 obj.image_size_pix obj.image_size_pix],...
                Screen('Rect',obj.window.pointer));
            
            obj.text_note_placement = obj.window.winRect(4) * .9;
            
            obj.cycles_per_flip = 1 / (obj.window.ifi * obj.flip_hz);
            
            % expecting max of 450 pixels
            obj.mondrian_size_pix = floor(obj.deg2pix(obj.mondrian_size_deg));
            
            % noise
            obj.noise_size_deg = obj.mondrian_size_deg;
            obj.noise_size_pix = obj.mondrian_size_pix;
            obj.noise_dst_rect = CenterRect([0 0 obj.noise_size_pix obj.noise_size_pix],...
                Screen('Rect', obj.window.pointer));
            
            
        end
                
    end
    
    methods (Access = protected)
        
        function drawFixation(obj)
            Screen('DrawLines', obj.window.pointer, obj.fix_rect, ...
                obj.fixLineSize, obj.fix_color, [obj.window.xCenter, obj.window.yCenter]);
        end
        
    end
    
    methods (Access = protected)
        
        function pixels = deg2pix(obj, desired_degrees )
            max_degrees = rad2deg(2 * atan( obj.window.screen_w_cm / obj.window.view_distance_cm ));
            pixels_per_degree = obj.window.winRect(3) / max_degrees;
            pixels = pixels_per_degree * desired_degrees;
        end
        
    end
    
end


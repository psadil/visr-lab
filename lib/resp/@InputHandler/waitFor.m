function response = waitFor(obj, whichkeys, inputemucode)
obj.prepare(whichkeys)
while 1
    response = obj.process_single_key(inputemucode);
    if ~isempty(response)
        obj.keys_pressed = [];
        obj.press_times = [];
        break;
    end
end
obj.shutdown();
end

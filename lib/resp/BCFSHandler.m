classdef BCFSHandler < InputHandler
    %InputHandler receive and parse inputs of various kinds from different
    %sources
    
    properties
        end_trial_key = {'Return', 'f', 'j', 'Space','RightArrow', 'LeftArrow'}
    end    
    
    methods
        function obj = BCFSHandler(responder)
            obj@InputHandler(responder);
        end
        
                
    end
    
    
end


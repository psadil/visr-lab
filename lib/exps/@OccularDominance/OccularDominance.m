classdef OccularDominance < Experiment
    %OccularDominance Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        exp = 'occudom';
        data
        n
        task
    end
    
    properties (Dependent)
        DominantEye
    end
    
    methods
        function obj = OccularDominance(input, askforsubnum)
            obj@Experiment(input, askforsubnum);
            
            obj.data = Data(input, obj.flut_basename, obj.exp);
            obj.n = size(obj.data.main,1);            
            obj.task = Arrow(obj.data, obj.window, obj.prompter, obj.input.responder);    
            
        end
        
        obj = run(obj)        
        
        function dominanteye = get.DominantEye(obj)
            %DominantEye: which eye saw the arrow more quickly, on average?  
            
            data_filter_caught = obj.data.main(~strcmpi(obj.data.main.exit_flag, 'CAUGHT'),:);            
            grpmeans = grpstats(data_filter_caught, 'arrow_to','nanmean', 'DataVars','rt');
            [~, I] = max(grpmeans.nanmean_rt);
            dominanteye = grpmeans.arrow_to(I);
        end
      
        give_instructions(obj)

        
    end
    
    
    methods (Access = private)
                
        arrowTex = selectArrowTex(stims, correct_direction)
                
    end
    
end


---
title: "Untitled"
author: "pss"
date: "February 19, 2018"
output: html_document
params:
  save: TRUE
  n_lists: 8 # first list is practice list
  n_objects_per_list: 18
  n_unit: 6
  max_study_dur_sec: 20
  max_binoc_dur_sec: 1 # in addition to whatever ramping occurs
  max_noise_dur_sec: 6
  hz: 10
  max_alpha: 0.6
  max_jitter_sec: 1
  ramp_dur_sec: 4
  seed: 08022018
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

set.seed(params$seed)

library(tidyverse)
library(magrittr)

n_object <- params$n_lists * params$n_objects_per_list
n_participant_per_unit <- 18
n_participant <-params$n_unit * n_participant_per_unit

ramp_dur_flips <- params$ramp_dur_sec * params$hz
max_jitter_flips <- params$max_jitter_sec * params$hz

# extra last flip to clear display
max_study_flips <- params$max_study_dur_sec * params$hz + ramp_dur_flips + 1
max_binoc_flips <- params$max_binoc_dur_sec * params$hz + ramp_dur_flips + 1
max_noise_flips <- params$max_noise_dur_sec * params$hz + ramp_dur_flips + 1

# objects_to_exclude <- c(5,9,10,12,17,21,22,24,27,35,38,36,41,42,46,48,49,51,54,55,
#             63,65,67,70,75,77,82,83,84,87,91,92,94,101,106,105,108,109,114,
#             116,120,127,130,132,134,136,138,142,148,149,152,158,160,164,168,169,
#             171,173,175,178,180, 182,
#             1,104, 81, 96, 151, 97,189, 19, 212, 215, 99, 56, 3, 6, 85, 25)


objects_to_exclude <- c(5,9,10,12,17,21,22,24,27,35,38,36,41,42,46,48,49,51,54,55,
            63,65,70,75,77,82,83,84,87,91,92,94,101,106,105,108,109,114,
            116,120,127,130,132,134,136,138,142,148,149,152,158,160,164,168,169,
            171,173,178,180, 182,
            1, 104, 81, 96, 151, 97, 189, 19, 212, 215, 99, 56, 3, 6, 85, 25, 199, 195, 89, 72, 145, 15, 203, 201)
practice_objects <- c(189, 19, 212, 215, 99, 56, 3, 6, 85, 25, 199, 195, 89, 72, 145, 15, 203, 201) 


object_names <-  readr::read_csv(file.path('objects', 'names.csv'))

stim_pairings <- readr::read_csv(file.path('objects','pairings.csv')) %>%
  filter(!(pair1 %in% objects_to_exclude)) %>%
  mutate(trial = 1:n()) %>%
  filter(trial <= n_object) %>%
  rename(object = pair1, pair = pair2) %>%
  mutate(name = object_names[object, ] %>% use_series("name1")) 


practice_pairings <- readr::read_csv(file.path('objects','pairings.csv')) %>%
  filter(pair1 %in% practice_objects) %>%
  mutate(trial = 1:n()) %>%
  filter(trial <= n_object) %>%
  rename(object = pair1, pair = pair2) %>%
  mutate(name = object_names[object, ] %>% use_series("name1")) 


```



```{r occular_dominance}

d_occular <- crossing(answer = c('\\LEFT', '\\RIGHT')
                      , arrow_to = c('left', 'right')
                      , rep = 1:10
                      , participant = 1:n_participant) %>%
  group_by(participant) %>%
  nest() %>%
  mutate(data = map(data, ~.x[sample(nrow(.x)),]),
         data = map(data, ~.x %>% mutate(trial = 1:nrow(.x)))) %>%
  unnest() %>%
  mutate(max_alpha = params$max_alpha,
         jitter = base::sample(1:max_jitter_flips, n(), replace = TRUE)
         , trial_start = NaN
         , trial_end = NaN
         , rt = NaN
)

if(params$save){
  readr::write_csv(d_occular, "task-occudom_blocking.csv")
}
```


```{r}

tInfo_occular <- d_occular %>%
  select(participant, trial, max_alpha, jitter, arrow_to, answer) %>%
  crossing(flip = 1:max_study_flips) %>%
  group_by(participant, jitter, max_alpha, arrow_to, answer, trial) %>%
  nest() %>%
  mutate(data = pmap(list(data=data, max_alpha=max_alpha, jitter = jitter),
                     function(data, max_alpha, jitter) {
                       data  %>% mutate(alpha = c(rep(0, each = jitter - 1),
                                                  seq(0, to = max_alpha, length.out = ramp_dur_flips),
                                                  rep(max_alpha, each = nrow(data) - jitter - ramp_dur_flips + 1)))
                     }
  )) %>%
  unnest() %>%
  mutate(vbl_realized = NaN
         , missed = NaN
  )


if(params$save){
  dir.create(file.path('fluts'), showWarnings = FALSE)

    for(s in unique(d_occular$participant)){
    tInfo_occular %>% filter(participant == s) %>%
      readr::write_csv(file.path('fluts', glue::glue("sub-{stringr::str_pad(s, 3, pad=0)}_task-occudom_flut.csv")))
  }
}
```


```{r}

# shuffle object orders, just to remove alphabetical component
d <- practice_pairings[sample(nrow(practice_pairings)), ] %>%
  bind_rows(stim_pairings[sample(nrow(stim_pairings)), ]) %>%
  mutate(list = rep(1:params$n_lists, each = params$n_objects_per_list)
         , trial_type = rep(1:params$n_objects_per_list, times = params$n_lists)) %>%
  mutate(expt = case_when(list == 1 ~ "practice"
                          , list > 1 ~ "visr")) %>%
  crossing(., unit = 1:params$n_objects_per_list) %>%
  group_by(unit) %>%
  nest() %>%
  crossing(rep_of_unit = 1:params$n_unit) %>%
  unnest() %>%
  group_by(unit, rep_of_unit) %>%
  nest() %>%
  mutate(participant = 1:n()) %>%
  unnest() %>%
  mutate(trial_type = 1 + ((trial_type + (participant %% params$n_objects_per_list)) %% params$n_objects_per_list ) ) %>% # increment trial_type for each participant
  group_by(participant, list) %>%
  nest() %>%
  mutate(data = map(data, ~.x[sample(nrow(.x)),]),
         data = map(data, ~.x %>% mutate(trial = 1:nrow(.x)))) %>% # extra shuffle for order of trial types
  unnest() %>%
  group_by(participant) %>%
  nest() %>%
  mutate(data = map(data, ~.x %>%
                      bind_rows(tibble(unit = NA, 
                                       rep_of_unit = NA, 
                                       object = 212, 
                                       pair = 215, 
                                       trial = 0, 
                                       name = 'lightswitch', 
                                       list = 1, 
                                       trial_type = 0, 
                                       expt = 'instructions')))) %>%
  unnest() %>%
  crossing(., phase = c("study", "name", "noise")) %>%
  mutate(condition = case_when(trial_type %in% 1:7 ~ "CFS"
                               , trial_type %in% 8:14 ~ "Not Studied"
                               , trial_type %in% 15:18 ~ "Binocular"
                               , trial_type == 0 ~ "CFS")
         , gonogo = case_when(trial_type %in% c(1, 8) ~ "name" # to be filtered out, redundant w/ phase==name & rep==2
                              , trial_type %in% c(2:3, 5:6, 9:10, 12:13, 15, 17) ~ "go"
                              , trial_type %in% c(4, 7, 11, 14, 16, 18) ~ "nogo"
                              , trial_type == 0 ~ "go")
         , cue = case_when(trial_type %in% c(1:4, 8:11, 15:16) ~ "present"
                           , trial_type %in% c(5:7, 12:14, 17:18) ~ "absent"
                           , trial_type == 0 ~ "present")
         , answer = case_when(condition == "CFS" & phase == "study" ~ "j"
                              , condition == "Not Studied" & phase == "study" ~ "f"
                              , phase == "name" ~ name
                              , phase == "noise" & gonogo == "go" ~ "\\ENTER"
                              , phase == "noise" & gonogo == "nogo" ~ '\\ENTER' # NOTE: this is not the correct answer. just done for ease of robo
                              , phase == "noise" & gonogo == "name" ~ name)
         , trial_start = NaN
         , trial_end = NaN
         , rt = NaN
         , pas = NaN
         , jitter = base::sample(1:max_jitter_flips, n(), replace = TRUE)
         , jitter = if_else(condition == "Binocular" & phase == "study", 0L, jitter)
         , jitter = if_else(condition == "Not Studied" & phase == "study", 0L, jitter)
         , jitter = if_else(gonogo == "nogo" & phase == "noise", 0L, jitter)
         , jitter = if_else(phase == "name", NA_integer_, jitter)
         , max_alpha = case_when(phase == "study" & condition == "CFS" ~ params$max_alpha
                                 , phase == "study" & condition == "Not Studied" ~ 0
                                 , phase == "study" & condition == "Binocular" ~ 1
                                 , phase == "name" ~ 1
                                 , phase == "noise" & gonogo == "go" ~ 1
                                 , phase == "noise" & gonogo == "nogo" ~ 0
                                 , phase == "noise" & gonogo == "name" ~ 1
                                 , phase == 'instructions' ~ 1)
  ) %>%
  crossing(rep = 1:2) %>% # study and name repeated, not noise
  filter(not(phase == "name" & rep == 2 & (gonogo == "go" | gonogo == "nogo")) & 
           not(phase == "noise" & rep == 2) &
           not(phase == 'instructions' & rep == 2)) 


if(params$save){
  readr::write_csv(d, "task-visr_blocking.csv")
}
```


All objects are studied twice
All objects are then named once
Objects may then be cued (noise, rep == 1)
 - For those objects that are cued, most will be in a 'go' trial, some will be in 'nogo' and some in a name (name, rep == 2)

For objects that are not cued
 - most will be in 'go', some will be 'nogo' (again, noise, rep==2 in both cases)
 


```{r}

tInfo_data <- d %>%
  select(phase, participant, list, trial, rep, condition, max_alpha, jitter, expt) %>%
  filter(phase %in% c("study", "noise")) %>%
  group_by(phase, condition, jitter) %>%
  nest() %>% #
  mutate(data = case_when(phase == "study" & condition == "Binocular" ~ map2(data, jitter, ~crossing(.x, flip = 1:(max_binoc_flips + .y)))
                          , phase == "study" & not(condition=="Binocular") ~ map2(data, jitter, ~crossing(.x, flip = 1:(max_study_flips + .y)))
                          , phase == "noise" ~ map2(data, jitter, ~crossing(.x, flip = 1:(max_noise_flips + .y) ))
  )) %>%
  unnest() %>%
  group_by(participant, list, rep, trial, phase, jitter, max_alpha, condition) %>%
  nest() %>%
  mutate(data = pmap(list(data=data, max_alpha = max_alpha, jitter = jitter),
                     function(data, max_alpha, jitter) {
                       data  %>% mutate(alpha = c(rep(0, each = jitter),
                                                  seq(0, to = max_alpha, length.out = ramp_dur_flips),
                                                  rep(max_alpha, each = nrow(data) - jitter - ramp_dur_flips - 1),
                                                  0 # shouldn't be needed
                                                  ))
                     }
  )) %>%
  unnest() %>%
  mutate(vbl_realized = NaN
         , missed = NaN
  )


if(params$save){
  for(s in unique(d$participant)){
    tInfo_data %>% filter(participant == s) %>%
      readr::write_csv(file.path('fluts', glue::glue("sub-{stringr::str_pad(s, 3, pad=0)}_task-visr_flut.csv")))
  }
}
```


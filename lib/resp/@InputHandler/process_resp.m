function obj = process_resp(obj, answer, trial_start)
switch obj.responder
    case 'simpleKeypressRobot'
        obj.inputemu('key_normal', answer);
end
obj = check_keys(obj);
if ~isempty(obj.keys_pressed)
    obj.rt = calc_rt(obj, trial_start);
    obj.response = clean_response(obj);
end
end

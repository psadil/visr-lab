classdef Data < handle
    %Data should probably have just been a connection to a db...
    %   Detailed explanation goes here
    
    properties
        main
        flut
    end
    
    methods
        function obj = Data(input, flut_basename, exp)
            
            switch exp
                case 'occudom'
                    obj.main = readtable(fullfile(input.root, 'stims', 'task-occudom_blocking.csv'));
                    obj.main = obj.main(obj.main.participant == input.subject,:);
                    N = size(obj.main,1);
                    
                    obj.main.eyes = repelem({[0,0]}, N)';
                    obj.main.eyes(strcmp(obj.main.arrow_to,{'left'})) = {[1,0]};
                    obj.main.eyes(strcmp(obj.main.arrow_to,{'right'})) = {[0,1]};
                    
                    obj.flut = readtable( [flut_basename, 'task-occudom_flut.csv']);
                case 'visr'
                    obj.main = readtable(fullfile(input.root, 'stims', 'task-visr_blocking.csv'));
                    obj.main = obj.main(obj.main.participant == input.subject,:);
                    N = size(obj.main,1);
                    
                    obj.main.eyes = repelem({[0,0]}, N)';
                    if strcmp(input.dominanteye, {'right'})
                        eye_fill = {[1,0]};
                    else
                        eye_fill = {[0,1]};
                    end
                    obj.main.eyes(strcmp(obj.main.condition,{'CFS'})) = eye_fill;
                    obj.main.eyes(strcmp(obj.main.condition,{'Binocular'})) = {[1,1]};
                    obj.main.pas = cell(N,1);
                    
                    obj.flut = readtable( [flut_basename, 'task-visr_flut.csv']);
            end
            
            obj.main.response = cell(N,1);
            obj.main.exit_flag = cell(N,1);
            
        end
    end
    
end


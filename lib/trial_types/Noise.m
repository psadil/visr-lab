classdef Noise < BCFS
    %Arrow Defines arrow facing one direction presented to one eye
    %   Detailed explanation goes here
    
    properties
        prompter
        bcfs_keys
    end
        
    methods
        function obj = Noise(data, window, prompter, responder)
            obj@BCFS(data, window, responder);
            
            obj.bcfs_keys = obj.keys.enter;
            
            obj.prompter = prompter;
                        
        end
        
        function texture = make_texes(obj)
            
            object = obj.data.main.object(obj.trial);
            pair = obj.data.main.pair(obj.trial);
            
            % grab all images
            [im, ~, alpha] = imread(fullfile(pwd, 'stims', 'objects', 'bullets', ...
                sprintf('object-%03d_paired-%03d_ap-1.png',object, pair)));
            
            % setup with alpha layer
            image = cat(3,im,alpha);
            
            % make textures of images
            texture = Screen('MakeTexture', obj.window.pointer, image);
        end
        
        function draw_stim(obj, prompt, image_tex, flip)
            
            noise_matrix = (50*rand(obj.noise_dst_rect(3) - obj.noise_dst_rect(1), ...
                obj.noise_dst_rect(3) - obj.noise_dst_rect(1)) + 128);
            noise_tex = Screen('MakeTexture', obj.window.pointer, noise_matrix);
            
            for eye = 1:2
                Screen('SelectStereoDrawBuffer', obj.window.pointer,eye-1);
                
                % draw same masking image to both eyes
                Screen('DrawTexture', obj.window.pointer, noise_tex);
                
                Screen('DrawTexture', obj.window.pointer, image_tex, [],...
                    obj.image_dst_rect, [], [], obj.data.flut.alpha(flip));
                
                % always draw central fixation cross
                obj.drawFixation();
                
                % prompt participant to respond
                DrawFormattedText(obj.window.pointer, prompt, 'center', obj.text_note_placement);
            end
            Screen('DrawingFinished', obj.window.pointer);
            
            Screen('Close', noise_tex);
            
        end
        
        
        function give_feedback(obj)
            switch obj.data.main.exit_flag{obj.trial}
                case 'CAUGHT'
                    obj.prompter.show({'INCORRECT! Please wait until you are certain!'});
                otherwise
                    switch obj.data.main.response{obj.trial}
                        case 'Return'
                            switch obj.data.main.gonogo{obj.trial}
                                case 'go'
                                    obj.prompter.show({sprintf('Correct! You found the object in %04d milliseconds', 1000*obj.data.main.rt(obj.trial))});
                                otherwise
                                    obj.prompter.show({'INCORRECT! There was no object.'});
                            end
                        otherwise
                            switch obj.data.main.gonogo{obj.trial}
                                case 'nogo'
                                    obj.prompter.show({'Correct! There was no object.'});
                                otherwise
                                    obj.prompter.show({'INCORRECT! There was an object.'});
                            end
                    end
            end
            
        end
        
        function give_instructions(obj)
            obj.prompter.show([{'This is practice for the second and final kind of memory tests!'},...
                {['On some trials, you will again see the partially hidden objects that you just named.\n',...
                'Try to think of the identity of each hidden object. Knowing the name may help you in your second task.\n',...
                '\nYou will sometimes be asked to type in the names again.']},...
                {['Other times, instead of being asked to provide the names, "salt and pepper" noise will appear.\n',...
                'An object may then emerge from that noise. When an object does appear, it will be the same one that you would have just tried to name.\n',...
                'When you notice an object emerging, your task will be to press Enter as quickly as you can.\n',...
                'However, if there is no object, just wait.\n',...
                '\nAn Example will be shown on the next slide']}, ...
                {'Press Enter only if you see an object'}
                ]);
            
            obj.trial = strcmpi(obj.data.main.expt, 'instructions') & ...
                strcmpi(obj.data.main.phase, 'noise');
            index_flut = find(strcmpi(obj.data.flut.phase, 'noise') & ...
                strcmpi(obj.data.flut.expt, 'instructions'));
            obj.run(index_flut, 'Press Enter if an object is present.');
            
            obj.prompter.show([{['If that had been a real trial, you would have wanted to press Enter.\n',...
                'Only press Enter when you see an object. Otherwise, just wait.']},...
                {['You will sometimes see the "salt and pepper" noise without first seeing the small part of the object.\n',...
                'In those cases, your task is the same. Press ENTER if you see an object, but do not respond if there is no object.']},...
                {'The practice will begin after you press SPACE'}]);
            
        end
        
    end
    
    
end


function obj = run(obj)

give_instructions(obj);

for trial = 1:obj.n
    obj.task.trial = trial;
    obj.task.run(find(obj.data.flut.trial == trial), ...
        'Which direction is arrow facing?'); %#ok<FNDSB>
    
    if mod(trial, 10)==0 && trial ~= obj.n
        obj.prompter.show([{['You have completed ', num2str(trial), ' out of ', num2str(obj.n), ' trials']},...
            {'Remember to keep your eyes focusd on the center white cross'}
            ]);
    end
    
    % inter-trial-interval
    WaitSecs(obj.iti_sec);
end

obj.prompter.shownowait({['That was the end of this first phase!\n' ,...
    'Hold on while the main experiment loads...']})

end

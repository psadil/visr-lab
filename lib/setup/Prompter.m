classdef Prompter < BaseStimulus
    %Prompter shows centered text and advances screen with response
    %   receives a matrix (possibly just one row) of cell arrays and
    %   displays each cell
    
    properties
        bcfshandler
    end
    
    methods
        function obj = Prompter(window, responder)
            obj@BaseStimulus(window);
            obj.bcfshandler = BCFSHandler(responder);
        end
        
        function show(obj, prompt)
            
            for p = 1:numel(prompt)
                vbl = GetSecs;
                
                for eye = 0:1
                    Screen('SelectStereoDrawBuffer', obj.window.pointer, eye);
                    DrawFormattedText(obj.window.pointer, prompt{p},...
                        'center', 'center');
                    DrawFormattedText(obj.window.pointer, '[Press SPACE to Continue]', ...
                        'center', obj.window.winRect(4)*.8);
                end
                Screen('DrawingFinished', obj.window.pointer);
                Screen('Flip', obj.window.pointer, vbl + obj.window.ifi/2 );
                
                obj.bcfshandler.waitForSpace();
                
            end
            
        end
        
        function shownowait(obj, prompt)
            
            for p = 1:numel(prompt)
                vbl = GetSecs;
                
                for eye = 0:1
                    Screen('SelectStereoDrawBuffer', obj.window.pointer, eye);
                    DrawFormattedText(obj.window.pointer, prompt{p},...
                        'center', 'center');
                end
                Screen('DrawingFinished', obj.window.pointer);
                Screen('Flip', obj.window.pointer, vbl + obj.window.ifi/2 );
                                
            end
            
        end
        
        
        % these methods do nothing!
        function [response, exit_flag] = clean_response(~)
            response = [];
            exit_flag = [];
        end
        
        function rt = calc_rt(~, ~)
            % RT is always based on first key press
            rt = [];
        end
        
    end
    
end


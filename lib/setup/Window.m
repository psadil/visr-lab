classdef Window < handle
    %Window Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
        screenNumber = 0;
        view_distance_cm = convlength(24,'in','m')*100;
        
        black = BlackIndex(Window.screenNumber);
        white = WhiteIndex(Window.screenNumber);
        gray = GrayIndex(Window.screenNumber);
        background = Window.gray;
        
    end
    
    
    properties
        
%         w_mm
%         h_mm
        oldRes
        screen_w_cm
        screen_h_cm
        pointer
        winRect
        xCenter
        yCenter
        width
        height
        
        ifi
        hertz
        slack
        
        TextFont
        TextSize
        TextStyle
        TextColor
        
    end
    
    methods
        function obj = Window(input)
            
%             [w_mm, h_mm] = Screen('DisplaySize', obj.screenNumber);
            
            % get screen resolution, set refresh rate
            obj.oldRes = Screen('Resolution', obj.screenNumber,[],[], input.refreshRate);
            
            % determine screen size with PTB
            obj.screen_w_cm = 53.34;
            obj.screen_h_cm = 29.89;
            
            PsychImaging('PrepareConfiguration');
%             PsychImaging('AddTask', 'General', 'NormalizedHighresColorRange', 1)
%             PsychImaging('AddTask', 'General', 'FloatingPoint32BitIfPossible');
            
            % Mondrians are drawn to offscreenwindow canvas
            % PsychImaging('AddTask','General','UseFastOffScreenWindows');
            
            Screen('Preference', 'SkipSyncTests', input.SkipSyncTests);
            switch input.debugLevel
                case 1
                    [obj.pointer, obj.winRect] = ...
                        PsychImaging('OpenWindow', obj.screenNumber, obj.background);
                case 10
                    PsychDebugWindowConfiguration(0, .5)
                    [obj.pointer, obj.winRect] = ...
                        PsychImaging('OpenWindow', obj.screenNumber,obj.background);
                case 0
                    ListenChar(-1);
                    HideCursor;
                    [obj.pointer, obj.winRect] = ...
                        PsychImaging('OpenWindow', obj.screenNumber, obj.background, [], [], [], input.stereomode);
            end
            
            % Make sure the GLSL shading language is supported:
            AssertGLSL;
            
            Screen('BlendFunction', obj.pointer, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');
            
            topPriorityLevel = MaxPriority(obj.pointer);
            Priority(topPriorityLevel);
            
            % define some landmark locations to be used throughout
            [obj.xCenter, obj.yCenter] = RectCenter(obj.winRect);
            
            % Get some the inter-frame interval, refresh rate, and the size of our window
            obj.ifi = Screen('GetFlipInterval', obj.pointer);
            obj.hertz = FrameRate(obj.pointer); % hertz = 1 / ifi
            obj.width = RectWidth(obj.winRect);
            obj.height = RectHeight(obj.winRect);
            
            % Font Configuration
            % Font Configuration
            obj.TextFont = 'Courier New';
            obj.TextSize = 24;
            obj.TextStyle = 1;
            obj.TextColor = obj.white;
            
            Screen('TextFont', obj.pointer, obj.TextFont);
            Screen('TextSize', obj.pointer, obj.TextSize);
            Screen('TextStyle', obj.pointer, obj.TextStyle);
            Screen('TextColor', obj.pointer, obj.TextColor);
            
            % how many frames of slack when scheduling flips (between 0-1)
            obj.slack = 0.5;
            
        end
        
        function checkRefreshRate(trueHertz, requestedHertz, constants)
            
            if abs(trueHertz - requestedHertz) > 2
                windowCleanup(constants);
                disp('Set the refresh rate to the requested rate')
            end
            
        end
        
        function delete(obj) %#ok<INUSD>
            
            ListenChar(0);
            Priority(0);
            sca; % alias for screen('CloseAll')
        end

                
    end
    
    
end

